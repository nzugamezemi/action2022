using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.IMGUI.Controls;
#endif

public class HierarchySeparator : Editor {

    [MenuItem("GameObject/Hierarchy Helper/Create Hierarchy Separator", false, 20)]
    [MenuItem("Tools/Hierarchy Helper/Create Hierarchy Separator")]
    static void CreateHierarchySeparator() {
        var go = new GameObject("=Separator");
        go.transform.position = new Vector3();
        go.SetActive(false);
    }

    internal static Event currentEvent;

    internal const int GLOBAL_SPACE_OFFSET_LEFT = 16 * 2;

    private static Texture2D pixelWhite;

    private static Texture2D PixelWhite {
        get {
            if (pixelWhite == null) {
                pixelWhite = new Texture2D(1, 1, TextureFormat.RGBA32, false);
                pixelWhite.SetPixel(0, 0, Color.white);
                pixelWhite.Apply();
            }

            return pixelWhite;
        }
    }

    private static bool useThemePlaymode = false;
    enum EditorTheme {
        Light, Dark, Playmode
    }
    static ThemeData lightTheme, darkTheme, playmodeTheme;

    static ThemeData usedTheme {
        get {
            if (EditorApplication.isPlayingOrWillChangePlaymode) {
                if (useThemePlaymode == false) {
                    playmodeTheme = EditorGUIUtility.isProSkin ? darkTheme : lightTheme;
                    playmodeTheme.BlendMultiply(GUI.color);

                    useThemePlaymode = true;
                }

                return playmodeTheme;
            } else {
                useThemePlaymode = false;
                return EditorGUIUtility.isProSkin ? darkTheme : lightTheme;
            }
        }
    }

    [InitializeOnLoadMethod]
    private static void Initialize() {
        EditorApplication.hierarchyWindowItemOnGUI += HierarchyOnGUI;
        EditorApplication.update += EditorUpdate;

        SetupThemeData();
    }

    private static void SetupThemeData() {
        lightTheme = new ThemeData();
        lightTheme.headerBackground = new Color(0.60f, 0.80f, 0.95f);
        lightTheme.headerTitle = new Color(0.043f, 0.043f, 0.043f);

        darkTheme = new ThemeData();
        darkTheme.headerBackground = new Color(0.30f, 0.60f, 0.75f);
        darkTheme.headerTitle = new Color(0.8235295f, 0.8235295f, 0.8235295f);
    }

    private static void EditorUpdate() {
        var activeObject = Selection.activeGameObject;
        if (activeObject) {
            Tools.hidden = activeObject.name.ToString().StartsWith("=");
        }
    }

    private static void HierarchyOnGUI(int instanceID, Rect selectionRect) {
        currentEvent = Event.current;

        var gameObject = EditorUtility.InstanceIDToObject(instanceID) as GameObject;

        // Sceneはreturn
        if (gameObject == null) {
            return;
        }

        // 仕切り用オブジェクト
        if (gameObject.ToString().StartsWith("=")) {
            Separator(gameObject.name, selectionRect);

            gameObject.hideFlags = HideFlags.None;

            // セパレータ選択時、選択から外す
            //var objects = Selection.objects;
            //objects = objects.Where((source, index) => index != objects.Length - 1).ToArray();
            //Selection.objects = objects;
        }
    }

    private static void Separator(string name, Rect selectionRect) {
        if (currentEvent.type != EventType.Repaint)
            return;

        // テキストカラー  
        var textColor = GetSeparatorTextColor();

        // エディタテーマによって背景色を変える
        var bgColor = GetSeparatorBackgroundColor();
        // var bgColor = new Color(0.7843138f, 0.7843138f, 0.7843138f);

        var rect = EditorGUIUtility.PixelsToPoints(RectFromLeft(selectionRect, Screen.width));
        rect.y = selectionRect.y;
        rect.height = selectionRect.height;
        rect.x += GLOBAL_SPACE_OFFSET_LEFT;
        rect.width -= GLOBAL_SPACE_OFFSET_LEFT;

        Color guiColor = GUI.color;
        GUI.color = bgColor;
        GUI.DrawTexture(rect, PixelWhite, ScaleMode.StretchToFill);

        var content = new GUIContent(name.ToString().Remove(0, 1));
        rect.x += (rect.width - Styles.Header.CalcSize(content).x) / 2;
        GUI.color = textColor;
        GUI.Label(rect, content, Styles.Header);
        GUI.color = guiColor;
    }

    static Rect RectFromLeft(Rect rect, float width) {
        rect.xMin = 0;
        rect.width = width;
        return rect;
    }

    internal static class Styles {

        internal static GUIStyle Header = new GUIStyle(TreeBoldLabel) {
            richText = true,
            normal = new GUIStyleState() { textColor = Color.white }
        };

        internal static GUIStyle TreeBoldLabel {
            get { return TreeView.DefaultStyles.boldLabel; }
        }
    }

    static Color GetSeparatorBackgroundColor() => usedTheme.headerBackground;
    static Color GetSeparatorTextColor() => usedTheme.headerTitle;

    [System.Serializable]
    public struct ThemeData {
        public Color headerTitle;
        public Color headerBackground;

        public ThemeData(ThemeData themeData) {
            headerTitle = themeData.headerTitle;
            headerBackground = themeData.headerBackground;
        }

        public void BlendMultiply(Color blend) {
            headerTitle = headerTitle * blend;
            headerBackground = headerBackground * blend;
        }
    }
}