using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class ColorProbe : MonoBehaviour
{

    Camera probeCamera;

    Texture2D tex;

    void GetRTPixels(RenderTexture rt)
    {
        // Remember currently active render texture
        RenderTexture currentActiveRT = RenderTexture.active;

        // Set the supplied RenderTexture as the active one
        RenderTexture.active = rt;

        if (!tex)
        {
            tex = new Texture2D(rt.width, rt.height);
        }
        else if (tex.width != rt.width || tex.height != rt.height)
        {
            tex.Reinitialize(rt.width, rt.height);
        }

        // Create a new Texture2D and read the RenderTexture image into it
        //Texture2D tex = new Texture2D(rt.width, rt.height);
        tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);

        // Restorie previously active render texture
        RenderTexture.active = currentActiveRT;
    }

    public Color GetColor(Vector3 normalizedScreenPosition)
    {
        return tex.GetPixel((int)(normalizedScreenPosition.x * tex.width), (int)(normalizedScreenPosition.y * tex.height));
    }

    // Start is called before the first frame update
    void Start()
    {
        probeCamera = GetComponent<Camera>();  
    }

    // Update is called once per frame
    void Update()
    {
        GetRTPixels(probeCamera.targetTexture);
    }
}
