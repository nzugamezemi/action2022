using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

[RequireComponent(typeof(Light2D))]
public class Light2DController : MonoBehaviour {

    private Light2D m_light2D;
    public Light2D Light2D {
        get {
            if (m_light2D == null) {
                TryGetComponent(out m_light2D);
            }
            return m_light2D;
        }
    }

    public Color[] colorPalette;

    [System.Serializable]
    public enum ColorType {
        None = -1, Red, Green, Blue
    }

    public Color GetColorFromColorType(ColorType targetColorType) {
        if (targetColorType == ColorType.None) {
            return Color.black;
        } else
            return colorPalette[(int)targetColorType];
    }

    [SerializeField] float changeSpeed = 10;
    ColorType prevColorType = ColorType.None;   // 色変更を検知する用
    public ColorType currentColorType = ColorType.None;

    private void Awake() {
        Init();
    }

    private void Init() {
        if (currentColorType != ColorType.None) ChangeLightColor(currentColorType);
    }

    public bool CheckColorChange() {
        bool result = false;
        if (prevColorType != currentColorType) {
            result = true;
            prevColorType = currentColorType;
        }

        return result;
    }

    /// <summary>
    /// ライトの色変更
    /// </summary>
    /// <param name="targetColor"></param>
    public void ChangeLightColor(ColorType targetColorType, bool useEasing = false) {
        currentColorType = targetColorType;
        if (useEasing) {
            StopCoroutine("ChangePlayerLightColorIE");
            StartCoroutine("ChangePlayerLightColorIE", targetColorType);
        } else {
            Light2D.color = GetColorFromColorType(targetColorType);
        }
    }

    IEnumerator ChangePlayerLightColorIE(ColorType targetColorType) {

        float judgeThreshould = 0.1f;
        Color targetColor = GetColorFromColorType(targetColorType);

        while (true) {

            // 色反映
            Light2D.color = Color.Lerp(Light2D.color, targetColor, Time.deltaTime * changeSpeed);

            var _c = Light2D.color;

            if (Mathf.Abs(targetColor.r - _c.r) < judgeThreshould &&
                Mathf.Abs(targetColor.g - _c.g) < judgeThreshould &&
                Mathf.Abs(targetColor.b - _c.b) < judgeThreshould
                ) {
                break;
            }

            yield return null;
        }

        currentColorType = targetColorType;

        yield break;
    }
}