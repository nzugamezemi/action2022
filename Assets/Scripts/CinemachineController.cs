using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CinemachineController : SingletonMonoBehaviour<CinemachineController> {

    [SerializeField]
    CinemachineImpulseSource m_impulseSource;

    public void GenerateImpulse(Vector3 velocity) {
        m_impulseSource.GenerateImpulseWithVelocity(velocity);
    }

}