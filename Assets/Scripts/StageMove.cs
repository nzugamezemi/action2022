using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class StageMove : MonoBehaviour
{
    public float speed;

    Animator animator;

    public void SetSpeed(float val)
    {
        speed = val;
    }

    //public Vector3 direction;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.speed = speed;
        //transform.Translate(direction.normalized*speed*Time.deltaTime);
    }
}
