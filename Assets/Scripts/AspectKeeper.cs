using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class AspectKeeper : SingletonMonoBehaviour<AspectKeeper> {

    [SerializeField]
    private Camera targetCamera; //対象とするカメラ

    [SerializeField]
    private Vector2 targetScreenSize = Vector3.one; //目的解像度
    public Vector2 TargetScreenSize => targetScreenSize;

    float screenAspect, targetAspect;

    void Update () {
        if (targetCamera && targetScreenSize != Vector2.zero) {
            screenAspect = (float)Screen.width / (float)Screen.height;   // 画面のアスペクト比
            targetAspect = targetScreenSize.x / targetScreenSize.y;   // 目的のアスペクト比

            ApplyScreenSize();
        }
    }

    void ApplyScreenSize () {
        // 目的アスペクト比にするための倍率
        var rateMultiplyer = targetAspect / screenAspect;

        // Viewport作成
        var viewportRect = new Rect(0, 0, 1, 1);

        if (rateMultiplyer < 1) {
            viewportRect.width = rateMultiplyer;   // 使用する横幅を変更
            viewportRect.x = 0.5f - viewportRect.width * 0.5f;   // 中央寄せ
        } else {
            viewportRect.height = 1 / rateMultiplyer;   // 使用する縦幅を変更
            viewportRect.y = 0.5f - viewportRect.height * 0.5f;   // 中央寄せ
        }

        targetCamera.rect = viewportRect;   // カメラのViewportに適用
    }
}