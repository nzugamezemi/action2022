using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Rendering.Universal;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ShadowCasterCircleTest : MonoBehaviour {

    private ShadowCaster2D shadowCaster;
    private CircleCollider2D circleCollider2D;
    private static BindingFlags accessFlagsPrivate =
        BindingFlags.NonPublic | BindingFlags.Instance;
    private static FieldInfo meshField =
        typeof(ShadowCaster2D).GetField("m_Mesh", accessFlagsPrivate);
    private static FieldInfo shapePathField =
        typeof(ShadowCaster2D).GetField("m_ShapePath", accessFlagsPrivate);
    private static MethodInfo onEnableMethod =
        typeof(ShadowCaster2D).GetMethod("OnEnable", accessFlagsPrivate);

    private int prev = 0;

    public int circleQuality = 20;

    public void UpdatePoints() {
        if (!Application.isPlaying && prev != circleQuality) {
            shadowCaster = GetComponent<ShadowCaster2D>();
            circleCollider2D = GetComponent<CircleCollider2D>();

            var circlePoints = new Vector3[circleQuality];
            // 角度差
            float angleDiff = 360f / circlePoints.Length;
            float _radius = circleCollider2D.radius;

            // 円状に配置
            for (int i = 0; i < circlePoints.Length; i++) {
                float angle = (90 - angleDiff * i) * Mathf.Deg2Rad;
                circlePoints[i] = new Vector3(_radius * Mathf.Cos(angle), _radius * Mathf.Sin(angle));
            }

            shapePathField.SetValue(shadowCaster, circlePoints);
            meshField.SetValue(shadowCaster, null);
            onEnableMethod.Invoke(shadowCaster, new object[0]);

            Debug.Log($"更新しました。頂点数 : [{circleQuality}]");

            prev = circleQuality;
        }
    }
}



#if UNITY_EDITOR
[CustomEditor(typeof(ShadowCasterCircleTest))]
class ShadowCasterCircleTestEditor : Editor {
    public override void OnInspectorGUI() {
        ShadowCasterCircleTest data = target as ShadowCasterCircleTest;

        GUILayout.Space(10);

        data.circleQuality = EditorGUILayout.IntField("円の頂点数", Mathf.Max(3, data.circleQuality));

        GUILayout.Space(10);

        if (GUILayout.Button("更新", GUILayout.Height(40))) {
            data.UpdatePoints();
        }
    }
}
#endif