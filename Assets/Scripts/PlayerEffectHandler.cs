using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEffectHandler : MonoBehaviour {

    [SerializeField] ParticleSystem damageEff;
    [SerializeField] Material damageEffMat;
    [SerializeField] Material damageRingEffMat;

    [Header("Trail")]
    [SerializeField] Material m_trailMat;
    [SerializeField] Material m_trailEmiMat;
    const string BASE_COLOR_PATH = "_BaseColor";
    const string EMISSION_COLOR_PATH = "_EmissionColor";

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        m_trailMat.SetColor(BASE_COLOR_PATH, GameManager.instance.playerController.light2DController.Light2D.color);
        m_trailEmiMat.SetColor(BASE_COLOR_PATH, GameManager.instance.playerController.light2DController.Light2D.color);
        m_trailEmiMat.SetColor(EMISSION_COLOR_PATH, GameManager.instance.playerController.light2DController.Light2D.color *64);
    }

    public void PlayEffect(string key="") {
        switch (key) {
            case "damage":
                damageEffMat.SetColor(BASE_COLOR_PATH, GameManager.instance.playerController.light2DController.Light2D.color);
                damageEffMat.SetColor(EMISSION_COLOR_PATH, GameManager.instance.playerController.light2DController.Light2D.color * 64);
                damageRingEffMat.SetColor(BASE_COLOR_PATH, GameManager.instance.playerController.light2DController.Light2D.color);
                damageRingEffMat.SetColor(EMISSION_COLOR_PATH, GameManager.instance.playerController.light2DController.Light2D.color * 64);
                damageEff.Play();
                break;
            default:
                return;
        }
    }
}