using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : SingletonMonoBehaviour<EnemyManager> {

    /// <summary>
    /// 敵の色用マテリアル
    /// </summary>
    public List<ColorList> colorList = new List<ColorList>();

    [System.Serializable]
    public class ColorList {
        public GameManager.ReactingColorType colorType;
        public Material material;
    }

    public Material GetEnemyColorAsMaterial(GameManager.ReactingColorType targetColorType) {
        foreach (var item in colorList) {
            if(item.colorType == targetColorType) {
                return item.material;
            }
        }

        return null;
    }
}