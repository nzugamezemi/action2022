using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : MonoBehaviour {

    PlayerController pController => GameManager.instance.playerController;
    PlayerEffectHandler pEffHandler => GameManager.instance.playerEffectHandler;

    bool m_isAlive = false;
    public bool isAlive => m_isAlive;

    [Header("体力")]
    [SerializeField] float m_maxHp = 100;
    float m_currentHp = 100;
    public float maxHp => m_maxHp;
    public float currentHp => m_currentHp;

    public float GetHpRate() => currentHp / maxHp;

    public bool useNaturalDecline = false;

    [SerializeField] UnityEngine.Events.UnityAction m_onDie;
    UnityEngine.Events.UnityAction onDie => m_onDie;

    // Start is called before the first frame update
    void Start() {
        useNaturalDecline = false;
        m_currentHp = m_maxHp;
        if (currentHp > 0) m_isAlive = true;
    }

    // Update is called once per frame
    void Update() {
        if (m_currentHp >= 0) {
            if (useNaturalDecline) m_currentHp -= Time.deltaTime;
            CheckDie();
        }

        if (UnityEngine.InputSystem.Keyboard.current.enterKey.wasPressedThisFrame) {
            GetDamage();
        }
    }

    public void GetDamage(float amount = 1) {
        if (!m_isAlive) return;

        // hp -= amount;
        m_currentHp -= 20;

        // パーティクル
        pEffHandler.PlayEffect("damage");

        // カメラシェイク
        CinemachineController.instance.GenerateImpulse(new Vector3(1, -2, -2));

        // Play SE
        {
            var smi = SoundManager.instance;
            if (smi)
            {
                smi.PlaySE(smi.playerDamage[Random.Range(0, smi.playerDamage.Count)]);
            }
        }

        CheckDie();
    }

    public void Heal(float amount = 1) {
        m_currentHp = Mathf.Clamp(m_currentHp + amount, 0, maxHp);
    }

    bool CheckDie() {
        if (currentHp <= 0) {
            m_currentHp = 0;

            Die();

            return true;
        } else
            return false;
    }

    public void OnDie(UnityEngine.Events.UnityAction callback) {
        m_onDie += callback;
    }

    /// <summary>
    /// 死亡処理
    /// </summary>
    void Die() {
        if (!m_isAlive) return;

        pController.anim.Play("Die");
        pController.enabled = false;
        GameManager.instance.ForcedStopStageAnimation();

        m_isAlive = false;

        UIManager.instance.DisplayGameOverCanvas();
        GameManager.instance.UnlockMouseCursor();

        // その他コールバック実行
        onDie?.Invoke();
    }
}