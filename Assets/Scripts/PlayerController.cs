using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

using System;
using System.Linq;

public class PlayerController : MonoBehaviour {

    [SerializeField] PlayerInputsReceiver input;

    [SerializeField] Rigidbody2D rb;

    [SerializeField] float moveSpeed = 5;

    Vector3 targetPos = new Vector3();

    [SerializeField] Animator m_anim;
    public Animator anim {
        get {
            if (m_anim == null) {
                m_anim = GetComponent<Animator>();
            }
            return m_anim;
        }
    }

    [Header("プレイヤーカラー設定")]
    [SerializeField] Light2DController m_light2DController;
    public Light2DController light2DController => m_light2DController;
    private int colorPaletteIndex = 0;

    [Header("フラッシュ")]
    [SerializeField] float lightIntensityMultiplierFlash = 2f;
    private float lightIntensityMultiplier = 1;
    float defaultLightIntensity = 0.5f;

    [Header("範囲内の敵")]
    public EnemyDetector enemyDetector;

    // Start is called before the first frame update
    void Start() {
        if (!input) TryGetComponent(out input);
        if (!rb) TryGetComponent(out rb);

        defaultLightIntensity = light2DController.Light2D.intensity;
    }

    // Update is called once per frame
    void Update() {
        if (input) {
            targetPos = GetScreenPointPosition(input.mousePosition);

            if (input.fire0) {
                // Change2PrevColor();

                if (enemyDetector.enemyList != null && enemyDetector.enemyList.Count > 0) {
                    List<EnemyController> el = new List<EnemyController>(enemyDetector.enemyList);
                    el.ForEach(e => e.RunAction());
                }

                    StopCoroutine("Flash");
                    StartCoroutine("Flash");

                //reset
                input.Fire0Input(false);
            }

            if (input.fire1) {
                // Change2NextColor();

                //reset
                input.Fire1Input(false);
            }

            /*if (input.fire2) {
                if (enemyDetector.enemiesList != null && enemyDetector.enemiesList.Count > 0) {
                    List<EnemyController> el = enemyDetector.enemiesList;
                    el.RemoveAll(i => i == null);
                    el.ForEach(i => i.Damage());
                }

                // reset
                input.Fire2Input(false);
            }*/
        }
    }

    Vector3 GetScreenPointPosition(Vector2 mousePosition) {
        var _pos = Camera.main.ScreenToWorldPoint(mousePosition);
        _pos.z = 0;
        return _pos;
    }

    private void FixedUpdate() {
        if (targetPos != Vector3.zero) {
            Move();
        }
    }

    private void OnDisable() {
        rb.velocity = Vector2.zero;
    }

    void Move() {
        rb.MovePosition(Vector2.Lerp(transform.position, targetPos, Time.deltaTime * moveSpeed));
    }

    void Change2NextColor() {
        if (m_light2DController && m_light2DController.colorPalette != null && m_light2DController.colorPalette.Length > 0) {
            if (colorPaletteIndex < m_light2DController.colorPalette.Length - 1) {
                colorPaletteIndex++;
            } else {
                colorPaletteIndex = 0;
            }

            m_light2DController.ChangeLightColor((Light2DController.ColorType)Enum.ToObject(typeof(Light2DController.ColorType), colorPaletteIndex), true);
        }
    }

    void Change2PrevColor() {
        if (m_light2DController && m_light2DController.colorPalette != null && m_light2DController.colorPalette.Length > 0) {
            if (colorPaletteIndex > 0) {
                colorPaletteIndex--;
            } else {
                colorPaletteIndex = m_light2DController.colorPalette.Length - 1;
            }

            m_light2DController.ChangeLightColor((Light2DController.ColorType)Enum.ToObject(typeof(Light2DController.ColorType), colorPaletteIndex), true);
        }
    }

    IEnumerator Flash () {
        lightIntensityMultiplier = 1;
        float _targetMul = lightIntensityMultiplierFlash;

        // 強く
        while (true) {
            yield return null;

            lightIntensityMultiplier = Mathf.Lerp(lightIntensityMultiplier, _targetMul, Time.deltaTime * 100f);

            light2DController.Light2D.intensity = defaultLightIntensity * lightIntensityMultiplier;

            if(Mathf.Abs(_targetMul - lightIntensityMultiplier) <= 0.1f) {
                break;
            }
        }

        yield return new WaitForSeconds(0.05f);

        // 弱く
        while (true) {
            yield return null;

            lightIntensityMultiplier = Mathf.Lerp(lightIntensityMultiplier, 1, Time.deltaTime * 5f);

            light2DController.Light2D.intensity = defaultLightIntensity * lightIntensityMultiplier;

            if (Mathf.Abs(1 - lightIntensityMultiplier) <= 0.1f) {
                break;
            }
        }

        light2DController.Light2D.intensity = defaultLightIntensity;

        yield break;
    }
}