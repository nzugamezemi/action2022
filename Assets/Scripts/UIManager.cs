using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : SingletonMonoBehaviour<UIManager> {

    [SerializeField] Slider hpGage;
    float hpFillAmount = 0;
    [SerializeField] TextMeshProUGUI hpText;

    [SerializeField] GameObject m_gameOverCanvas;
    public GameObject gameOverCanvas => m_gameOverCanvas;

    [SerializeField] GameObject m_gameClearCanvas;
    public GameObject gameClearCanvas => m_gameClearCanvas;
    [SerializeField] List<Image> numberDisplayList = new List<Image>();
    [SerializeField] List<Sprite> allNumberSpriteList = new List<Sprite>();

    private void Update() {
        hpFillAmount = UpdateHpImgAmount(GameManager.instance.playerStatus.GetHpRate());
        UpdateHpText();
    }

    float UpdateHpImgAmount(float amount) {
        return hpGage.value = Mathf.Clamp01(amount);
    }

    string UpdateHpText() {
        string _str = $"{Mathf.RoundToInt(hpFillAmount * 100)}";
        return hpText.text = _str;
    }

    public void DisplayGameOverCanvas() {
        if (gameOverCanvas) gameOverCanvas.SetActive(true);
    }

    public void DisplayGameClearCanvas() {
        if (gameClearCanvas) gameClearCanvas.SetActive(true);
    }

    public void ScoreToSpriteNumber(int score) {
        string numStr = score.ToString("D4");

        if (score == 0) return;

        int[] arr = new int[4];
        for (int i = 0; i < numStr.Length; i++) {
            arr[i] = int.Parse(numStr[i].ToString());
        }

        for (int i = 0; i < 4; i++) {
            numberDisplayList[i].sprite = allNumberSpriteList[arr[i]];
        }
    }
}