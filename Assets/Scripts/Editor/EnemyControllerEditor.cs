using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(EnemyController))]
public class EnemyControllerEditor : Editor {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        EnemyController data = target as EnemyController;

        EditorGUI.BeginChangeCheck();

        //data.detector.reactingColorType
        var colT = data.detector.reactingColorType;
        var _mat = EnemyManager.instance.GetEnemyColorAsMaterial(colT);
        data.SetEnemySpriteMaterial(_mat);

        if (EditorGUI.EndChangeCheck()) {
            var scene = SceneManager.GetActiveScene();
            EditorSceneManager.MarkSceneDirty(scene);
        }
    }
}