using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(Light2DController))]
public class Light2DControllerEditor : Editor {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        Light2DController data = target as Light2DController;

        EditorGUI.BeginChangeCheck();

        //data.detector.reactingColorType
        if (data.CheckColorChange()) {
            Debug.Log("Change");
            data.ChangeLightColor(data.currentColorType);
        }

        if (EditorGUI.EndChangeCheck()) {
            var scene = SceneManager.GetActiveScene();
            EditorSceneManager.MarkSceneDirty(scene);
        }
    }
}