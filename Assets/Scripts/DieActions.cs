using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DieActions : MonoBehaviour
{
    public UnityEvent onDie;

    public void OnDie()
    {
        onDie?.Invoke();
    }
}
