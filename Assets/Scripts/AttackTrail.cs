using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackTrail : MonoBehaviour
{

    public TrailRenderer renderer;

    public void SetTrailPositions(Vector3[] positions) {
        renderer.SetPositions(positions);
    }

    public void ActivateTrail() {
        renderer.enabled = true;
    }
}
