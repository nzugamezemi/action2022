using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : SingletonMonoBehaviour<SoundManager> {

    public AudioSource bgmSource;
    public AudioSource seSource;

    [Header("BGM Clips")]
    public AudioClip tutorial;
    public AudioClip main;
    public AudioClip clear;

    [Header("SE Clips")]
    public List<AudioClip> playerDamage = new List<AudioClip>();
    public List<AudioClip> enemyDamage = new List<AudioClip>();
    public AudioClip retryBtn;
    public AudioClip quitBtn;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    public void PlaySE(AudioClip clip) {
        seSource.PlayOneShot(clip);
    }

    public void PlayBGM(AudioClip clip) {
        bgmSource.clip = clip;
        bgmSource.Play();
    }
}