using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FadeCanvasHandler : MonoBehaviour
{

    public Animator anim;

    public UnityEvent onCompleted;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnCompleted() {
        onCompleted.Invoke();

        onCompleted.RemoveAllListeners();
    }
}
