using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class GameManager : SingletonMonoBehaviour<GameManager> {

    [SerializeField] private List<Light2DController> _allLight2DController;
    public List<Light2DController> AllLight2DController {
        get {
            if (_allLight2DController == null || _allLight2DController.Count == 0)
            {
                _allLight2DController = FindObjectsOfType<Light2DController>().ToList();
            }
            return _allLight2DController;
        }
    }

    private PlayerController m_playerController;
    public PlayerController playerController {
        get {
            if (m_playerController == null)
            {
                m_playerController = FindObjectOfType<PlayerController>();
            }
            return m_playerController;
        }
    }

    private PlayerStatus m_playerStatus;
    public PlayerStatus playerStatus {
        get {
            if (m_playerStatus == null)
            {
                m_playerStatus = FindObjectOfType<PlayerStatus>();
            }
            return m_playerStatus;
        }
    }

    private PlayerEffectHandler m_playerEffectHandler;
    public PlayerEffectHandler playerEffectHandler {
        get {
            if (m_playerEffectHandler == null)
            {
                m_playerEffectHandler = FindObjectOfType<PlayerEffectHandler>();
            }
            return m_playerEffectHandler;
        }
    }

    [SerializeField] GameObject m_stageContainer;
    [SerializeField] Animator m_stageContainerAnimator;

    // 扱う色
    [System.Serializable]
    public enum ReactingColorType {
        Shadow,
        EveryLights,
        Red,
        Green,
        Blue,
        Yellow,
        Cyan,
        Magenta,
        White
    }

    /// <summary>
    /// RGBに分解
    /// </summary>
    /// <param name="reactingColorType"></param>
    /// <returns></returns>
    public List<Light2DController.ColorType> GetDisassembledArrayFromReactingColorType(ReactingColorType reactingColorType) {
        switch (reactingColorType)
        {
            case ReactingColorType.Shadow:
                return null;
            case ReactingColorType.EveryLights:
                return null;
            case ReactingColorType.Red:
                return new List<Light2DController.ColorType>() { Light2DController.ColorType.Red };
            case ReactingColorType.Green:
                return new List<Light2DController.ColorType>() { Light2DController.ColorType.Green };
            case ReactingColorType.Blue:
                return new List<Light2DController.ColorType>() { Light2DController.ColorType.Blue };
            case ReactingColorType.Yellow:
                return new List<Light2DController.ColorType>() { Light2DController.ColorType.Red, Light2DController.ColorType.Green };
            case ReactingColorType.Cyan:
                return new List<Light2DController.ColorType>() { Light2DController.ColorType.Green, Light2DController.ColorType.Blue };
            case ReactingColorType.Magenta:
                return new List<Light2DController.ColorType>() { Light2DController.ColorType.Blue, Light2DController.ColorType.Red };
            case ReactingColorType.White:
                return new List<Light2DController.ColorType>() { Light2DController.ColorType.Red, Light2DController.ColorType.Blue, Light2DController.ColorType.Green };
            default:
                return null;
        }
    }

    int m_score;
    public int score {
        get {
            if (m_score < 0)
            {
                m_score = 0;
            }
            return m_score;
        }
        private set {
            m_score = value;
            if (m_score < 0)
            {
                m_score = 0;
            }
        }
    }

    // メニューを操作したか
    bool runnedMenuEvent = false;

    bool gameCleared = false;

    void Start() {
        // ターゲットフレームレート : 60FPS
        SetTargetFrameRate(60);

        LockMouseCursor();

        m_stageContainer.SetActive(false);
        m_stageContainerAnimator.enabled = false;

        runnedMenuEvent = false;
        gameCleared = false;
    }

    private void SetTargetFrameRate(int f) {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = f;
    }

    /// <summary>
    /// カーソルロック & 非表示
    /// </summary>
    public void LockMouseCursor() {
        // カーソル画面内ロック
        Cursor.lockState = CursorLockMode.Confined;
        // カーソル非表示
        Cursor.visible = false;
    }


    /// <summary>
    /// カーソルロック解除 & 表示
    /// </summary>
    public void UnlockMouseCursor() {
        // カーソル画面内ロック解除
        Cursor.lockState = CursorLockMode.None;
        // カーソル表示
        Cursor.visible = true;
    }

    void Update() {
        if (UnityEngine.InputSystem.Keyboard.current.spaceKey.wasPressedThisFrame)
        {
            GameClear();
        }
    }

    public void ForcedStopStageAnimation() {
        if (m_stageContainerAnimator) m_stageContainerAnimator.enabled = false;
    }

    public void AddScore(int amount) {
        score += amount;
    }

    public void GameRetry() {
        if (runnedMenuEvent) return;

        runnedMenuEvent = true;

        SoundManager.instance.PlaySE(SoundManager.instance.retryBtn);

        Invoke("ReloadCurrentScene", 0.5f);
    }

    /// <summary>
    /// シーン再読み込み
    /// </summary>
    public void ReloadCurrentScene() {
        var sceneName = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(sceneName);
    }

    public void GameQuit() {
        if (runnedMenuEvent) return;

        runnedMenuEvent = true;

        SoundManager.instance.PlaySE(SoundManager.instance.quitBtn);

        Invoke("GoTitle", 0.5f);
    }

    void GoTitle() {
        SceneManager.LoadScene("TitleScene");
    }

    public void GameStart() {
        playerStatus.useNaturalDecline = true;

        m_stageContainer.SetActive(true);
        m_stageContainerAnimator.enabled = true;
        m_stageContainerAnimator.Play("StageAnimation");

        SoundManager.instance.PlayBGM(SoundManager.instance.main);
    }

    public void GameClear() {
        if (gameCleared) return;

        gameCleared = true;

        Debug.Log("GameClear");

        UIManager.instance.ScoreToSpriteNumber(score);

        UIManager.instance.DisplayGameClearCanvas();

        SoundManager.instance.PlayBGM(SoundManager.instance.clear);
    }
}