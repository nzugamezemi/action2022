using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ColorDebug : MonoBehaviour
{
    public TextMeshProUGUI label;
    public ColorProbe probe;
    public Image panel;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (label && probe)
        {
            Vector3 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
            screenPosition.Scale(new Vector3(1f / Camera.main.pixelWidth, 1f / Camera.main.pixelHeight, 1));
            Color screenColor = probe.GetColor(screenPosition);

            label.text = screenColor.ToString();

            if (panel)
            {
                panel.color = screenColor;
            }
        }
    }
}
