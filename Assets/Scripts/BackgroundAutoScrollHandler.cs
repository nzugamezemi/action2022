using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class BackgroundAutoScrollHandler : MonoBehaviour {

    const float DEFAULT_SCROLL_SPEED = .1f;

    [Header("自動スクロール設定")]
    float prevScrollSpeed;
    [SerializeField] bool allowRealtimeSpeedChange = true;
    [SerializeField] float scrollSpeed = DEFAULT_SCROLL_SPEED;
    [SerializeField] SpriteRenderer[] backgrounds;

    // Start is called before the first frame update
    void Start() {
        ApplyScrollSpeed(scrollSpeed);
    }

    // Update is called once per frame
    void Update() {
        if (allowRealtimeSpeedChange) {
            if (scrollSpeed != prevScrollSpeed) {
                // Debug.Log("Change");
                ApplyScrollSpeed(scrollSpeed);
                prevScrollSpeed = scrollSpeed;
            }
        }
    }

    void ApplyScrollSpeed(float newSpeed = DEFAULT_SCROLL_SPEED) {
        if (backgrounds == null || backgrounds.Length == 0) return;

        for (int i = 0; i < backgrounds.Length; i++) {
            scrollSpeed = newSpeed;
            backgrounds[i].sharedMaterial.SetFloat("_ScrollSpeed", scrollSpeed);
        }
    }
}