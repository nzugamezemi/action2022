using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundEffectDirection : MonoBehaviour {

    [SerializeField] Transform stageContainer;

    Vector2 prevStagePos;

    [SerializeField] Transform effect;

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (stageContainer&& effect) {
            Vector2 currentStagePos = stageContainer.position;

            Vector2 direction = prevStagePos - currentStagePos;

            prevStagePos = currentStagePos;
        }
    }
}