using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class TitleManager : MonoBehaviour {

    [SerializeField]FadeCanvasHandler fadeCanvasHandler;

    bool readyToStart = false;

    private InputAction _pressAnyKeyAction =
                new InputAction(type: InputActionType.PassThrough, binding: "*/<Button>", interactions: "Press");

    private void OnEnable() => _pressAnyKeyAction.Enable();
    private void OnDisable() => _pressAnyKeyAction.Disable();

    private AsyncOperation async;

    // Start is called before the first frame update
    IEnumerator Start() {
        fadeCanvasHandler.anim.speed = 0.5f;
        fadeCanvasHandler.onCompleted.AddListener(()=> {
            readyToStart = true;
        });
        fadeCanvasHandler.anim.CrossFadeInFixedTime("FadeIn", 0.01f);

        async = SceneManager.LoadSceneAsync("MainScene");
        async.allowSceneActivation = false;

        yield break;
    }

    // Update is called once per frame
    void Update() {
        if (readyToStart && _pressAnyKeyAction.triggered && async.progress >= 0.9f) {
            readyToStart = false;

            fadeCanvasHandler.anim.speed = 0.5f;
            fadeCanvasHandler.onCompleted.AddListener(() => {
                async.allowSceneActivation = true;
            });
            fadeCanvasHandler.anim.CrossFadeInFixedTime("FadeOut", 0.01f);
        }
    }
}