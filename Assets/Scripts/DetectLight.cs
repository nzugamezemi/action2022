using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// LINQ使用
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class DetectLight : MonoBehaviour {

    [SerializeField] private float radius = 0.5f;
    [SerializeField] private Vector2 scale = Vector2.one;

    // Raycastに使う形状の種類
    enum Type {
        Circle,
        Points
    }
    // 形状選択用
    [SerializeField] private Type type;

    [SerializeField, Header("検知する色")]
    public GameManager.ReactingColorType reactingColorType;

    public void ChangeReactColor(GameManager.ReactingColorType newType) {
        reactingColorType = newType;
    }

    // 当たっているライト
    public List<Light2DController> activeLights { get; private set; }
    public bool exitActiveLights => activeLights != null && activeLights.Count > 0;

    public bool isReacting { get; private set; }

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        // 当たっているライトをリスト化
        activeLights = UpdateActiveLightsStats();

        // 色を判定し、反応を返すか
        isReacting = UpdateReactingState();
    }

    private bool UpdateReactingState() {
        // ライトに当たっている場合
        if (activeLights != null && activeLights.Count > 0) {
            // 全てのライトに反応する場合
            if (reactingColorType == GameManager.ReactingColorType.EveryLights) {
                return true;
            }

            // 当たってるライトのカラー情報をリスト化
            var _colorTypes = new List<Light2DController.ColorType>();
            for (int i = 0; i < activeLights.Count; i++) {
                _colorTypes.Add(activeLights[i].currentColorType);
            }
            // 一致するか
            return MatchColors(reactingColorType, _colorTypes);
        } else {
            // ライトに当たっていなくても、黒に反応するなら true を返す
            /*if (reactingColorType == GameManager.ReactingColorType.Black) {
                return true;
            } else {
                return false;
            }*/
            return reactingColorType == GameManager.ReactingColorType.Shadow;
        }
    }

    private List<Light2DController> UpdateActiveLightsStats() {
        List<Light2DController> _activeLights = new List<Light2DController>();

        var origin = transform.position;
        foreach (var light in GameManager.instance.AllLight2DController) {
            if (InLight(origin, light)) _activeLights.Add(light);
        }

        return _activeLights;
    }

    // 一つのライトを指定し、
    // 「ライトの範囲内か」 -> 加えて「間に障害物はないか」を検知
    private bool InLight(Vector2 origin, Light2DController lightController) {
        Vector2 lightPosition = lightController.transform.position;
        Vector2 dir = lightPosition - origin;

        // ライト範囲外ならfalse返して中断
        if (!LightIsWithinRange(lightController.Light2D.shapeLightFalloffSize, lightPosition, origin)) return false;

        // ここでカラー判別はしない

        // 障害物検知
        switch (type) {
            case Type.Circle:
                Vector2 crossVec1, crossVec2;

                crossVec1 = new Vector2(dir.y, -dir.x).normalized * radius;
                crossVec2 = new Vector2(-dir.y, dir.x).normalized * radius;

                RaycastHit2D[] _hits_c = new RaycastHit2D[3];

                _hits_c[0] = Physics2D.Linecast(origin, lightPosition);
                _hits_c[1] = Physics2D.Linecast(origin + crossVec1, lightPosition);
                _hits_c[2] = Physics2D.Linecast(origin + crossVec2, lightPosition);

                return _hits_c.All(value => value == false);
            case Type.Points:
                Vector2[] points = new Vector2[] {
                    new Vector2(scale.x ,scale.y),
                    new Vector2(-scale.x ,scale.y),
                    new Vector2(scale.x ,-scale.y) ,
                    new Vector2(-scale.x ,-scale.y)
                };

                RaycastHit2D[] _hits_b = new RaycastHit2D[points.Length];

                for (int i = 0; i < _hits_b.Length; i++) {
                    _hits_b[i] = Physics2D.Linecast(origin + points[i] / 2, lightPosition);
                }

                return _hits_b.All(value => value == false);
            default:
                return false;
        }
    }

    /// <summary>
    /// ライトの範囲内かどうかを返す
    /// </summary>
    /// <param name="fallSize"></param>
    /// <param name="lightPosition"></param>
    /// <param name="origin"></param>
    /// <returns></returns>
    private bool LightIsWithinRange(float fallSize, Vector2 lightPosition, Vector2 origin) {
        // 三平方の定理のルート計算はCPUに負荷がかかるのでsqrMagnitudeを使う
        return Mathf.Pow(fallSize, 2) >= (lightPosition - origin).sqrMagnitude;
    }

    /// <summary>
    /// 当たっているライトの色が反応用の色と一致しているかを返す
    /// </summary>
    /// <param name="reactingColorType"></param>
    /// <param name="activeLights"></param>
    /// <returns></returns>
    private bool MatchColors (GameManager.ReactingColorType reactingColorType, List<Light2DController.ColorType> activeLights) {
        var _cArr = GameManager.instance.GetDisassembledArrayFromReactingColorType(reactingColorType);
        var _aArr = activeLights.Distinct().ToList();
        if (_cArr != null && _cArr.Count > 0 && _aArr != null && _aArr.Count > 0) {
            _cArr.Sort();
            _aArr.Sort();
            return _cArr.SequenceEqual(_aArr);
        } else {
            return false;
        }
    }

    private void OnDrawGizmos() {

        switch (type) {
            case Type.Circle:

                Gizmos.color = Color.red;
                Gizmos.DrawWireSphere(transform.position, radius);

                break;
            case Type.Points:
                Vector2[] points = new Vector2[] {
                    new Vector2(scale.x ,scale.y),
                    new Vector2(-scale.x ,scale.y),
                    new Vector2(scale.x ,-scale.y) ,
                    new Vector2(-scale.x ,-scale.y)
                };

                Gizmos.color = Color.red;
                Gizmos.DrawWireCube(transform.position, scale);

                for (int i = 0; i < points.Length; i++) {
                    Gizmos.DrawSphere((Vector2)transform.position + points[i] / 2, 0.04f);
                }

                break;
        }
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(DetectLight))]
public class DetectLightColorEditor : Editor {
    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        DetectLight _data = target as DetectLight;

        // EditorGUILayout.LabelField($"光に居るか : {(_data.inShadow ? "影" : "光に当たっている")}");
    }
}
#endif