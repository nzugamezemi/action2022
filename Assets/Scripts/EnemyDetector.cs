using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetector : MonoBehaviour {

    public float radius = 0;
    float tmpRadius = 0;

    public List<EnemyController> enemyList;

    public Light2DController playerLight2DController;

    // Start is called before the first frame update
    void Start() {
        radius = playerLight2DController.Light2D.shapeLightFalloffSize;
    }

    // Update is called once per frame
    void Update() {
        if (radius != tmpRadius) {
            radius = playerLight2DController.Light2D.shapeLightFalloffSize;
            tmpRadius = radius;
        }
    }

    public void CheckAndUpdateEnemyListInArea(EnemyController eCon) {
        if (!eCon) return;

        if (Vector3.Distance(playerLight2DController.transform.position, eCon.transform.position) < radius) {
            if (!enemyList.Contains(eCon)) {
                enemyList.Add(eCon);
            }
        } else {
            if (enemyList.Contains(eCon)) {
                enemyList.Remove(eCon);
            }
        }
    }

    public void RemoveEnemyFromList(EnemyController eCon) {
        if (enemyList.Contains(eCon)) {
            enemyList.Remove(eCon);
        }
    }
}