using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputsReceiver : MonoBehaviour {

	[Header("Player Input Values")]
	public Vector2 mousePosition;

	public bool fire0;
	public bool fire1;
	public bool fire2;

	public void OnMousePosition(InputAction.CallbackContext context) {
		MousePositionInput(context.ReadValue<Vector2>());
	}

	public void OnFire0(InputAction.CallbackContext context) {
		Fire0Input(context.performed);
    }

	public void OnFire1(InputAction.CallbackContext context) {
		Fire1Input(context.performed);
    }

	public void OnFire2(InputAction.CallbackContext context) {
		Fire2Input(context.performed);
    }

	public void MousePositionInput(Vector2 newMousePosition) {
		mousePosition = newMousePosition;
	}

	public void Fire0Input(bool newState) {
		fire0 = newState;
	}

	public void Fire1Input(bool newState) {
		fire1 = newState;
	}

	public void Fire2Input(bool newState) {
		fire2 = newState;
	}
}