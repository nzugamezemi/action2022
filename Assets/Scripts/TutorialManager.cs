using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TutorialManager : MonoBehaviour {

    bool isPlayingTutorial = false;

    [Header("UI Elements")]
    [SerializeField]Canvas canvas;
    [SerializeField]Animator canvasAnim;
    [SerializeField]TextMeshProUGUI uiText;
    [SerializeField]RectTransform textRTrns;

    [Header("Stage Elements")]
    [SerializeField] Animator stageAnim;
    [SerializeField] EnemyController en1, en2;
    bool enemy1Died = false, enemy2Died = false;
    [SerializeField] EnemyController en3;
    bool enemy3Died = false;

    private void Start() {
        SoundManager.instance.PlayBGM(SoundManager.instance.tutorial);

        StartCoroutine("Tutorial");
    }

    private void Update() {
        if (isPlayingTutorial && UnityEngine.InputSystem.Keyboard.current.escapeKey.wasPressedThisFrame || UnityEngine.InputSystem.Mouse.current.middleButton.wasPressedThisFrame) {
            Debug.Log("チュートリアルスキップ");
            EndTutorial();
        }
    }

    IEnumerator Tutorial() {
        yield return new WaitForSeconds(0.5f);

        isPlayingTutorial = true;

        canvas.gameObject.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        uiText.text = "マウスを動かして移動してみよう";
        canvasAnim.Play("FadeInText");   // IN

        yield return new WaitForSeconds(3f);

        canvasAnim.Play("FadeOutText");   // OUT

        yield return new WaitForSeconds(1.5f);

        uiText.text = "左クリックで 「攻撃」 が発動するよ";
        canvasAnim.Play("FadeInText");   // IN

        yield return new WaitForSeconds(5f);

        canvasAnim.Play("FadeOutText");   // OUT

        yield return new WaitForSeconds(1f);

        enemy1Died = false;
        enemy2Died = false;
        en1.isInvincible = true;
        en2.isInvincible = true;

        stageAnim.Play("TutorialStage01");

        yield return new WaitForSeconds(1f);

        uiText.text = "白い敵は「光にいれて」攻撃";
        canvasAnim.Play("FadeInText");   // IN

        yield return null;
        yield return new WaitUntil(() => stageAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.90f);

        yield return new WaitForSeconds(0.5f);

        en1.isInvincible = false;
        en2.isInvincible = false;

        yield return new WaitUntil(() => (enemy1Died && enemy2Died));

        canvasAnim.Play("FadeOutText");   // OUT

        yield return new WaitForSeconds(1f);



        enemy3Died = false;
        en3.isInvincible = true;
        en3.dieOnlyWhileReacting = true;

        stageAnim.Play("TutorialStage02");

        yield return new WaitForSeconds(1f);

        uiText.text = "黒い敵は「影にいれて」攻撃";
        canvasAnim.Play("FadeInText");   // IN

        yield return null;
        yield return new WaitUntil(() => stageAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 0.90f);

        yield return new WaitForSeconds(0.5f);

        en3.isInvincible = false;

        yield return new WaitUntil(() => enemy3Died);

        canvasAnim.Play("FadeOutText");   // OUT

        yield return new WaitForSeconds(1f);



        uiText.text = "↑ HPは自然減少 + 被ダメージで減るよ";
        textRTrns.anchoredPosition = new Vector2(-540, 380);
        canvasAnim.Play("FadeInText");   // IN

        yield return null;

        yield return new WaitForSeconds(5f);

        canvasAnim.Play("FadeOutText");   // OUT

        yield return new WaitForSeconds(1f);

        uiText.text = "- ゲーム開始 -";
        textRTrns.anchoredPosition = new Vector2(0, -50);
        canvasAnim.Play("FadeInText");   // IN

        yield return null;
        stageAnim.Play("TutorialStage_Out");

        yield return new WaitForSeconds(2f);

        canvasAnim.Play("FadeOutText");   // OUT

        yield return new WaitForSeconds(1f);

        EndTutorial();

        yield break;
    }

    public void EndTutorial() {

        StopCoroutine("Tutorial");

        // ゲーム開始
        GameManager.instance.GameStart();

        isPlayingTutorial = false;

        stageAnim.gameObject.SetActive(false);
        canvas.gameObject.SetActive(false);
    }

    public void OnTutorialEnemyDied (int i) {
        switch (i) {
            case 1:
                enemy1Died = true;
                break;
            case 2:
                enemy2Died = true;
                break;
            case 3:
                enemy3Died = true;
                break;
        }
    }
}