using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class RotateEnemy : MonoBehaviour {

    [SerializeField] bool m_previewInEditor = false;

    [SerializeField] float radius = 2f;

    [SerializeField] float rotateSpeed = 1f;

    float x, y;   // position

    [SerializeField] Transform pivotOverrideTrns;
    [SerializeField] EnemyController enemy;

    // Start is called before the first frame update
    void Start() {
        x = 0;
    }

    // Update is called once per frame
    void Update() {
        if (m_previewInEditor || Application.isPlaying) {
            Rotate();
        }
    }

    void OnRenderObject() {
#if UNITY_EDITOR
        if (!Application.isPlaying) {
            EditorApplication.QueuePlayerLoopUpdate();
            SceneView.RepaintAll();
        }
#endif
    }

    public void Rotate() {
        if (enemy && !enemy.allowChaseMode) {
            x = radius * Mathf.Sin(Time.time * rotateSpeed);   // X軸の設定
            y = radius * Mathf.Cos(Time.time * rotateSpeed);   // Z軸の設定

            Vector2 center;
            if (pivotOverrideTrns) {
                center = pivotOverrideTrns.position;
            } else {
                center = transform.position;
            }

            enemy.transform.position = center + new Vector2(x, y);
        }
    }
}