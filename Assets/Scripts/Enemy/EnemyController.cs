using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    bool isAlive = true;
    public bool isInvincible = false;

    Animator anim;

    public string movementState = "EnemyStill";

    [SerializeField] GameObject visual;

    [SerializeField] ParticleSystem fadeEff;
    [SerializeField] ParticleSystemRenderer fadeEffMat;
    [SerializeField] ParticleSystem dieEff;
    [SerializeField] ParticleSystemRenderer dieEffMat;
    [SerializeField] ParticleSystemRenderer dieEffRingMat;

    [SerializeField] AttackTrail attackTrail;

    [SerializeField] DetectLight m_detector;
    public DetectLight detector {
        get {
            if (m_detector == null) {
                TryGetComponent(out m_detector);
            }
            return m_detector;
        }
    }

    [SerializeField] float moveSpeed = 2f;
    public void SetMoveSpeed(float newValue) {
        moveSpeed = newValue;
    }
    public float GetMoveSpeed() => moveSpeed;

    [SerializeField] SpriteRenderer m_spriteRenderer;
    public void SetEnemySpriteMaterial(Material newMat) {
        if (m_spriteRenderer) m_spriteRenderer.material = newMat;
    }

    [SerializeField]
    float fadeSpeed = 5;

    public bool allowChaseMode = false;
    bool chasing = false;

    private bool playerHealed = false;

    [SerializeField] int score = 50;

    [HideInInspector]
    public bool dieOnlyWhileReacting = false;

    private void Start() {
        isAlive = true;
        chasing = false;

        anim = GetComponent<Animator>();
        if (anim != null) {
            var stateId = Animator.StringToHash(movementState);
            var hasState = anim.HasState(0, stateId);
            if (hasState) {
                anim.Play(movementState);
            } else {
                Debug.LogError($"ステート名[{movementState}] のアニメーションステートが存在しません。\n綴りなど確認してください。");
            }

            anim.enabled = false;
        }
    }

    private void Update() {
        if (!isAlive) {
            if (!fadeEff.isPlaying && !dieEff.isPlaying && Mathf.Approximately(transform.localScale.x, 0)) {
                Destroy(gameObject);
            }
            return;
        }

        GameManager.instance.playerController.enemyDetector.CheckAndUpdateEnemyListInArea(this);

        // Chasing
        if (allowChaseMode) {
            Chasing();
        }
    }

    public void BeginChase() {
        if (!allowChaseMode) return;

        if (!chasing) {
            anim.enabled = false;
            var _re = GetComponent<RotateEnemy>();
            if (_re) _re.enabled = false;

            transform.SetParent(null, true);

            attackTrail.ActivateTrail();

            chasing = true;
        }
    }

    void Chasing() {
        if (chasing && !Mathf.Approximately(transform.localScale.x, 0)) {
            var dir = GameManager.instance.playerController.transform.position - transform.position;
            transform.position = Vector3.MoveTowards(transform.position, GameManager.instance.playerController.transform.position, 50 * Time.deltaTime);
            // transform.Translate(dir.normalized * 100 * Time.deltaTime);

            if (Mathf.Pow(0.5f, 2) > dir.sqrMagnitude) {
                GameManager.instance.playerStatus.GetDamage();
                Die(playDieEff: false);
            }
        }
    }

    private void Die(bool playDieEff = true) {
        isAlive = false;

        // Remove this from player's list
        GameManager.instance.playerController.enemyDetector.RemoveEnemyFromList(this);

        attackTrail.transform.SetParent(null);

        // Deactive visible
        {
            transform.localScale = new Vector3();
            visual.SetActive(false);
        }

        // Play SE
        {
            var smi = SoundManager.instance;
            if (smi) {
                smi.PlaySE(smi.enemyDamage[Random.Range(0, smi.enemyDamage.Count)]);
            }
        }

        // Play die effects
        {
            if (playDieEff) {
                dieEffMat.material.SetColor("_BaseColor", m_spriteRenderer.material.GetColor("_BaseColor"));
                dieEffMat.material.SetColor("_EmissionColor", m_spriteRenderer.material.GetColor("_BaseColor") * 2f);
                dieEffRingMat.material.SetColor("_BaseColor", m_spriteRenderer.material.GetColor("_BaseColor"));
                dieEffRingMat.material.SetColor("_EmissionColor", m_spriteRenderer.material.GetColor("_BaseColor") * 10);
                dieEff.transform.rotation = Quaternion.LookRotation(transform.position - GameManager.instance.playerController.transform.position);
                dieEff.Play();
            }
        }

        // Heal player
        {
            if (!playerHealed) {
                GameManager.instance.playerStatus.Heal(5);
                var _c = GetComponent<DieActions>();
                if (_c) _c.OnDie();
                playerHealed = true;
            }
        }

        GameManager.instance.AddScore(score);
    }

    public void RunAction() {
        if (isInvincible) return;

        if (dieOnlyWhileReacting && !detector.isReacting) return;

        if (detector.isReacting) {
            Die();
        } else {
            BeginChase();
        }
    }
}